package nu.westlin.kotlinspringboot

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@WebMvcTest(nu.westlin.kotlinspringboot.GreetingController::class)
class GreetingControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @MockBean
    lateinit var greetingService: GreetingService

    @Test
    fun greet_shouldGreetUnknown() {
        val name = "Fisksoppa"
        val content = Greeting("Service says \"Hello, unknown!\"")

        given(greetingService.greet(name)).willReturn(content)

        this.mockMvc.perform(get("/greet/$name")).andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(content)))
    }

    @Test
    fun greet_shouldGreetUser() {
        val name = "Fisksoppa"
        val content = Greeting("Service says \"Hello, $name!\"")

        given(greetingService.greet(name)).willReturn(content)

        this.mockMvc.perform(get("/greet/$name")).andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(content)))
    }
}