package nu.westlin.kotlinspringboot

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@RunWith(SpringRunner::class)
@WebMvcTest(nu.westlin.kotlinspringboot.UserController::class)
class UserControllerTest {

    val user1 = User(1, "JUnit1")
    val user2 = User(2, "JUnit2")
    val users = listOf(user1, user2)

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @MockBean
    lateinit var userService: UserService

    @Test
    fun allUsers_noParams() {
        given(userService.getAll()).willReturn(users)

        this.mockMvc.perform(get("/user").contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(users)))
    }

    @Test
    fun allUsers_id1ShouldReturnUser1() {
        given(userService.getAll()).willReturn(users)

        this.mockMvc.perform(get("/user").contentType(MediaType.APPLICATION_JSON).param("id", user1.id.toString()))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(listOf(user1))))
    }

    @Test
    fun allUsers_nameJUnit2ShouldReturnUser2() {
        given(userService.getAll()).willReturn(users)

        this.mockMvc.perform(get("/user").contentType(MediaType.APPLICATION_JSON).param("name", user2.name))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(listOf(user2))))
    }

    @Test
    fun allUsers_id1AndNameJUnit1ShouldReturnUser2() {
        given(userService.getAll()).willReturn(users)

        this.mockMvc.perform(get("/user").contentType(MediaType.APPLICATION_JSON).param("id", user1.id.toString()).param("name", user1.name))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(listOf(user1))))
    }

    @Test
    fun userByName_shouldReturnUser() {
        val user: User = users[0]
        given(userService.get(user.name)).willReturn(user)

        this.mockMvc.perform(get("/user/${user.name}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(user)))
    }

    @Test
    fun userByName_userNotFoundShouldReturn404AndEmptyBody() {
        val name = "sfghw4667wv4"
        given(userService.get(name)).willReturn(null)

        this.mockMvc.perform(get("/user/$name").contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isNotFound).andExpect(content().string(""))
    }

    @Test
    fun createUser() {
        val username = "Pingu"
        val user = User(1, username)

        given(userService.createUser(username)).willReturn(user)

        this.mockMvc.perform(post("/user").contentType(MediaType.TEXT_PLAIN).content(username))
                .andDo(print()).andExpect(status().isOk).andExpect(content().json(objectMapper.writeValueAsString(user)))
    }

    @Ignore("Mockito throws an exception that I just can't seem to fix :(")
    @Test
    fun createUser_usernameAlreadExistShouldReturn409() {
        val username = "Pingu"
        Mockito.`when`(userService.createUser(username)).thenThrow(DuplicateUsernameException(username))
        //Mockito.doThrow(DuplicateUsernameException(username)).`when`(userService).createUser(username)
        //given(userService.createUser(username)).willThrow(DuplicateUsernameException(username))

        this.mockMvc.perform(post("/user").contentType(MediaType.TEXT_PLAIN).content(username))
                .andDo(print()).andExpect(status().isConflict).andExpect(content().string(""))
    }

}