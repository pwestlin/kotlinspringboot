package nu.westlin.kotlinspringboot

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class UserServiceTest {
    val service: UserService = UserService()

    @Test
    fun exist_trueForExistingUser() {
        for (user in service.getAll()) {
            assertThat(service.exist(user.name), `is`(true))
        }
    }

    @Test
    fun exist_falseForNonExistingUser() {
        assertThat(service.exist("aefgh426hw4gv4w7"), `is`(false))
    }

    @Test
    fun get_shouldReturnUser() {
        for (user in service.getAll()) {
            assertThat(service.get(user.name), `is`(user))
        }
    }

    @Test
    fun get_shouldReturnNull() {
        assertThat(service.get("sfgae65w"), `is`(nullValue()))
    }

    @Test
    fun getAll() {
        assertThat(service.getAll().size, `is`(2))
    }

    @Test
    fun createUser() {
        val username = "Pingu"
        val user = service.createUser(username)

        assertThat(user, `is`(User(service.getAll().size.toLong(), username)))
    }

    @Test(expected = DuplicateUsernameException::class)
    fun createUser_alreadyExistingUsernameShouldThrowException() {
        val username = "Pingu"
        service.createUser(username)
        service.createUser(username)
    }
}

