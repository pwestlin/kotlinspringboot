package nu.westlin.kotlinspringboot

import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class GreetingServiceTest {

    lateinit var greetingService: GreetingService

    @Mock
    lateinit var userService: UserService

    @Before
    fun init() {
        greetingService = GreetingService(userService)
    }

    @Test
    fun greet_shouldGreetUser() {
        val name = "Sune"
        given(userService.exist(name)).willReturn(true)

        assertThat(greetingService.greet(name), `is`(Greeting("Service says \"Hello, $name!\"")));
    }

    @Test
    fun greet_shouldGreetUnknown() {
        val name = "Sune"
        given(userService.exist(name)).willReturn(false)

        assertThat(greetingService.greet("aweroghae"), `is`(Greeting("Service says \"Hello, unknown!\"")));
    }
}