package nu.westlin.kotlinspringboot

import org.springframework.stereotype.Service

@Service
open class GreetingService(val userService: UserService) {

    fun greet(name: String): Greeting {
        val username = if (userService.exist(name)) name else "unknown"

        return Greeting("Service says \"Hello, $username!\"")
    }
}

@Service
// TODO This is more of an repository...
open class UserService {
    internal val users = mutableListOf(User(1, "Peter"), User(2, "Sune"))

    fun exist(name: String) = users.find { it.name == name } != null

    fun get(name: String): User? {
        try {
            return users.filter { it.name == name }.first()
        } catch(e: NoSuchElementException) {
            return null
        }
    }

    fun createUser(username: String): User {
        if (users.find { it.name == username } != null) throw DuplicateUsernameException("Username $username already exist.")
        
        val user = User(createId(), username)
        users.add(user)

        return user
    }

    protected fun createId() = users.size + 1L

    fun getAll(): List<User> = users
}

open class DuplicateUsernameException(message: String?) : Throwable(message)