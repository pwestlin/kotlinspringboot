package nu.westlin.kotlinspringboot


data class User(val id: Long, val name: String)

data class Greeting(val text: String)

