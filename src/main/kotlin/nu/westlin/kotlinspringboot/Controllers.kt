package nu.westlin.kotlinspringboot

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.MediaType.TEXT_PLAIN_VALUE
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/")
open class HomeController {
    @GetMapping
    fun home(): String {
        return "Hello from Kotlin and Spring boot!"
    }
}

@RestController
@RequestMapping("/greet")
open class GreetingController(val greetingService: GreetingService) {

    @GetMapping("/{name}")
    fun greet(@PathVariable name: String) = greetingService.greet(name)
}

@RestController
@RequestMapping("/user")
open class UserController(val userService: UserService) {

    @GetMapping(produces = arrayOf(APPLICATION_JSON_VALUE))
    fun allUsers(@RequestParam id: Long?, @RequestParam name: String?, response: HttpServletResponse): List<User> {
        return userService.getAll()
                .filter { id == null || id == it.id }
                .filter { name == null || name == it.name }
    }

    @GetMapping("/{name}", produces = arrayOf(APPLICATION_JSON_VALUE))
    fun userByName(@PathVariable name: String, response: HttpServletResponse): User? {
        val user = userService.get(name)

        if (user == null) {
            response.status = HttpStatus.NOT_FOUND.value()
        }

        return user
    }

    @PostMapping(consumes = arrayOf(TEXT_PLAIN_VALUE), produces = arrayOf(APPLICATION_JSON_VALUE))
    fun createUser(@RequestBody username: String, response: HttpServletResponse): User? {
        var user: User? = null

        try {
            user = userService.createUser(username)
        } catch(e: DuplicateUsernameException) {
            response.status = HttpStatus.CONFLICT.value()
        }

        return user
    }

    // Just for playing around with automatic JSON-mapping :)
    @PostMapping("/check", consumes = arrayOf(APPLICATION_JSON_VALUE), produces = arrayOf(APPLICATION_JSON_VALUE))
    fun checkUser(@RequestBody user: User, response: HttpServletResponse) {
        println("user = $user")
    }

}
